CREATE DATABASE Hanesegi
USE Hanesegi 

CREATE TABLE customers(
cust_id char(5) not null primary key,
cust_name varchar(50))

CREATE TABLE orders(
order_num char(5) not null primary key,
order_date DATE not null,
cust_id char(5) not null,
foreign key(cust_id) references customers(cust_id))

CREATE TABLE vendors(
vend_id char(5) not null primary key,
vend_name varchar(50) not null)

CREATE TABLE products(
prod_id char(5) not null primary key,
prod_name varchar(50),
prod_price int,
stock int,
vend_id char(5) not null,
FOREIGN KEY(vend_id) REFERENCES vendors(vend_id))

CREATE TABLE orderitems(
order_num char(5) not null,
prod_id char(5) not null,
quantity int,
foreign key(order_num) references orders(order_num),
foreign key(prod_id) references products(prod_id))

CREATE TABLE log_products(
prod_id char(5) not null,
status_prod varchar(10)	not null,
status datetime DEFAULT GETDATE())

INSERT INTO log_products(prod_id,status_prod) VALUES
('P0001','added')

select * from log_products	

*/ Soal1 

CREATE TRIGGER simpan_produk
ON products
AFTER INSERT
AS
BEGIN
	DECLARE @id char(5)
	SELECT @id = prod_id from inserted

	INSERT INTO log_products (prod_id, status_prod)
	VALUES (@id, 'added');
END;

SELECT * FROM simpan_produk


INSERT INTO products values
('P0009','Masin Keti',550000,29,'V0004')
('P0015',' Speaker Simbadda',450000,100,'V0003')

SELECT * FROM products


INSERT INTO vendors values
('V0004','Hanes')
('V0003','Yohanes')


*/ Soal2

CREATE TRIGGER hapus_produk
ON products
AFTER INSERT
AS
BEGIN
	DECLARE @id char(5)
	SELECT @id = prod_id from inserted

	INSERT INTO log_products (prod_id, status_prod)
	VALUES (@id, 'deleted');
END;

DELETE FROM products WHERE prod_id = 'P0009'
select * from products

*/SOAL3

CREATE TRIGGER simpan_orderitems
ON orderitems
AFTER INSERT
AS
BEGIN
	DECLARE @id char(5)
	SELECT @id = prod_id from inserted

	INSERT INTO	products(prod_id,stock)
	VALUES (@id, 'stock berkurang');
END;

*/soalno4

CREATE TRIGGER hapus_orderitems
ON orderitems
AFTER INSERT
AS
BEGIN
	DECLARE @id char(5)
	SELECT @id = prod_id from inserted

	INSERT INTO	products(prod_id,stock)
	VALUES (@id, 'stock bertambah');
END;
