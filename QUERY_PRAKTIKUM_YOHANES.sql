CREATE DATABASE PBD_yohanesegipratamayudoutomo
use PBD_yohanesegipratamayudoutomo


--1. Buatlah Database dan Strukur Tabel

create table pelanggan
(
KodePelanggan char(5) primary key,
NamaPelanggan varchar(25) not null,
AlamatPelanggan varchar(50) not null
)

create table barang
(
KodeBarang char(5) primary key,
NamaBarang varchar(25) not null,
HargaBarang int not null,
StokBarang int not null,
Satuan varchar(15) not null
)

create table kwitansi
(
NomorKwitansi char(5) primary key,
TanggalKwitansi date not null,
KodePelanggan char(5) not null foreign key
references
pelanggan(KodePelanggan)
)create table detail_kwitansi
(
NomorKwitansi char(5) not null foreign key
references
kwitansi(NomorKwitansi),
KodeBarang char(5) not null foreign key
references
barang(KodeBarang),
jumlahjual int not null
)--2. Tampilkan Relasi Antar Tabel (ada disebelah tab tampilanya);--3. Simpan data berikut ke dalam tabel pelangganINSERT INTO pelanggan VALUES('P0090','Dhani','Bandar Lampung'),('P0091','Ahmad','Lampung Selatan'),('P0092','Ali','Lampung Timur');select * from pelanggan--4. Ubah nama pelanggan yang semula Dhani menjadi DoniUPDATE pelanggan SET NamaPelanggan = 'Doni' WHERE KodePelanggan = 'P0090'--5. Ubah data pelanggan dengan kode P0092 menjadi Nama = adi, alamat = Bandar LampungUPDATE pelanggan SET NamaPelanggan = 'Adi', AlamatPelanggan = 'Bandar Lampung' WHERE KodePelanggan = 'P0092'--6. Hapus data pada tabel pelanggan yang memiliki kode P0091DELETE  FROM pelanggan  WHERE KodePelanggan = 'P0091'--7. Simpan data berikut ke dalam tabel pelangganinsert into pelanggan values
('P0001','aditya','Bandar Lampung'),
('P0002','nandi','Lampung Barat'),
('P0003','indrajat','Lampung Selatan'),
('P0004','ali','Bandar Lampung'),
('P0005','agung p','Lampung Selatan'),
('P0006','dedi','Lampung Selatan'),
('P0007','rhendy','Lampung Selatan'),
('P0008','lulus','Lampung Timur'),
('P0009','alipi','Bandar Lampung'),
('P0010','indra','Bandar Lampung'),
('P0011','wigo','Bandar Lampung'),
('P0012','andika','Bandar Lampung'),
('P0013','alfian','Bandar Lampung'),
('P0014','fahmi','Lampung Barat'),
('P0015','afrand','Bandar Lampung'),
('P0016','ebi','Bandar Lampung'),
('P0017','agung','Bandar Lampung'),
('P0018','mahono','Bandar Lampung'),
('P0019','yuda','Bandar Lampung'),
('P0020','arsy','Lampung Timur'),
('P0021','nurman','Lampung Selatan'),
('P0022','yogi','Bandar Lampung'),
('P0023','apriyani','Bandar Lampung'),
('P0024','latiful','Bandar Lampung'),
('P0025','bima','Lampung Barat'),
('P0026','bagas','Bandar Lampung'),
('P0027','aska','Bandar Lampung'),
('P0028','alfian','Bandar Lampung'),
('P0029','kevin','Bandar Lampung'),
('P0030','jala','Lampung Timur'),
('P0031','apry','Lampung Timur'),
('P0032','grenci','Bandar Lampung'),
('P0033','niko','Bandar Lampung'),
('P0034','rindu','Lampung Barat')--Check Tabel Pelangganselect * from pelanggan--8. Simpan minimal 30 data barang ke dalam file excel yang disimpan menjadi file data_barang.csv (comma delimeted)--Sudah Selesai--9. Import file .csv ke dalam SQL SERVERBULK INSERT barang
FROM 'D:\Tugas Kuliah\Semester 4\Pemrograman Basis Data\Praktikum\Pertemuan 4\barang.csv'
WITH
(
FIRSTROW = 2,
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n'
)
GO

-- SOAL 

--1. Tampilkan Seluruh data pada tabel Pelanggan
SELECT * FROM pelanggan
--2. Tampilkan seluruh data Nama dan Alamat pada tabel pelanggan
SELECT NamaPelanggan, AlamatPelanggan From pelanggan
--3. Tampilkan berasal dari alamat (kota) mana saja pelanggan yang ada di tabel pelanggan
SELECT DISTINCT AlamatPelanggan FROM pelanggan
--4. Tampilkan seluruh data pada tabel pelanggan yang beralamat di Bandar Lampung
SELECT * FROM pelanggan WHERE AlamatPelanggan = 'Bandar Lampung'
--5. Tampilkan seluruh data pada tabel pelanggan yang tidak beralamat di bandar lampung
SELECT * FROM pelanggan WHERE NOT AlamatPelanggan = 'Bandar Lampung'
--6. Tampilkan seluruh data pada tabel pelanggan yang memiliki nama depan huruf �a�
SELECT * FROM pelanggan WHERE NamaPelanggan  LIKE 'a%'
--7. Tampilkan seluruh data pada tabel pelanggan yang memiliki nama belakang dengan akhiran �i� yang beralamat di Bandar Lampung
SELECT * FROM pelanggan WHERE NamaPelanggan LIKE '%i' AND AlamatPelanggan = 'Bandar Lampung'
--8. Tampilkan seluruh data pada tabel barang yang memiliki harga lebih dari 500.000
SELECT * FROM barang
SELECT * FROM barang WHERE HargaBarang >500000
--9. Tampilkan data berupa nama barang, harga, stok dan satuan pada tabel barang yang memilki nama depan huruf �H� atau �A� yang memiliki harga lebih dari 3 juta dan memiliki satuan pcs
SELECT * From barang
SELECT * FROM barang WHERE NamaBarang LIKE 'H%' OR NamaBarang LIKE 'A%' OR HargaBarang >3000000
--10. Tampilkan seluruh data pada tabel barang yang memiliki harga antara 500.00 sampai 1.500.000 yang diurutkan secara descending
SELECT * FROM barang WHERE HargaBarang BETWEEN	500000 AND 1500000 ORDER BY HargaBarang DESC;